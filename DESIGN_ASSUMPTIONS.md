# Design and Assumptions

### How to run locally
To run the project you need to have .net and dependencies installed.  
You also need a redis server running, see appsettings.json in case
you need to change any connection information for your local setup.
```bash
cd src/ExchangeRates.WebApi
dotnet watch run
```
To run unit tests
```bash
cd src/ExchangeRates.Tests
dotnet test
```

### Configuration
The system has been designed so it can be entirely configured
using appsettings.json. There is a Fixer configuration for the fixer api
and an exchange rate configuration to control the list of base currencies 
and supported currencies.

### Rates
There is a Task scheduler which runs background tasks for making the requests
to the fixer REST api to get the latest currencies. This is run independently 
from the Web API to retrieve a conversion rate. The idea is that the Web API
will respond immediately and fast whether the data is stale or up to date.   
This background task will then be re fetching the latest rates based on a
configurable update policy. This setup will work for both free and paid version
of fixer since we have a list of base currencies that will be used to fetch
conversion rates from that base currency to all of the supported currencies.  
If using free version then you will only define EUR in base currencies since
free version only supports EUR and also have your data expiry set to a minimum 
of 60 minutes since free version updates every 60 minutes.
If you decide to upgrade to paid version you can add the extra base currencies
into the config and the background tasks will fetch those 
conversion rates as well without having to change any logic.


### Rate conversions
The algorithm for converting base currency into a target currency works
as follows. Given Currencies A, B and C. We have the following configuration.  
BaseCurrencies = [A]  
Currencies = [A, B, C]  
Fixer api will return us the conversion for A -> B and A -> C.  
From this we will determine the conversion for B -> A and C -> A.   
This algorithm is B -> A = 1 / (A -> B).  
Example:  
A -> B = 0.5  
B -> A = 1 / 0.5 = 2 
  
  
Then we check if we have a base currency for B and C. If we have a base currency
then no further conversions are made since this will be handled by another background
task with the specific conversion for that base currency, if we don't then we run through another 
algorithm to determine the conversion for  B -> C.  
This algorithm is B -> C = (A -> C) / (A -> B).    
Example:  
A -> B = 0.5  
A -> C = 0.25  
B -> C = 0.25 / 0.5  = 0.5

### Data storage
Since the data storage is used more as a proxy between the application
and fixer api and it can be re hydrated at any time I decided on using Redis
to store the fetched data and all conversion rates. In order to support 
this being in a load balanced environment a simple check was implemented
in the background task requesting the data to check if a request has already
been stored and if so skip over to the next request. In a larger system
you would probably want to have a distributed task queue that is handling
the distribution of tasks between all servers.  
After the rates have been fetched and converted they are then stored into Redis
where the key == {BaseCurrency}_{TargetCurrency}. Then when the api is 
requesting they can do a simple key lookup to get the data.  

### Dependency Injection
Dependency injection is done using Constructor injection 
with the built in .net core DI. Since this is built into core there was no need
to use another DI library. All of the services etc used which contain 
their own independent logic are separated. An interface
is created for each of these services which is injected into the constructor 
of the other service/controller etc which depends on it. Since these are now 
separated we can easily mock their implementation in unit tests and change 
internal implementation details without affecting the dependant services etc.  

### Web API 
A web api endpoint has been created to retrieve the conversion rate.  
It uses query parameters for baseCurrency and targetCurrency. Based 
on the requirements of the contract definition if this was a Post request
I would have made the request body accept a json payload with the exact 
contract. Since it was specified to use Get request this was simplified 
to use query parameters instead of passing an 
encoded json string to a query parameter.  

The request is setup to handle Validation on the request object and will
respond appropriately if fields are not defined etc. A custom validator
was also setup as an additional validation to check whether a requested
currency is supported by the application.  

An edge case may potentially happen if the endpoint is requesting a conversion
rate which has not yet been fetched by the background task. This would only
ever happen on the first initial startup of the Application where no
data has previously been stored in Redis. A retry check 
has been put in place for this particular scenario so that the endpoint will
wait for a maximum of N tries for this data to be ready. If after all tries 
have not resolved then an exception will be thrown with a 
conversion rate unavailable at this time message.  
  
**Endpoints**  
* Conversion rate for base and target currency.  
  https://localhost:5001/api/RetrieveExchangeRate?baseCurrency=EUR&targetCurrency=AUD  
  
* Unsupported conversion rate.  
  https://localhost:5001/api/RetrieveExchangeRate?baseCurrency=EUR&targetCurrency=unknown  
  
* List all conversion rates for convenience.  
  https://localhost:5001/api/RetrieveExchangeRate/list  
  

### Async
All logic has been implemented with Async in mind so we don't block threads.

### Unit tests
A unit test project has been created which tests all of the logic.

### Load testing
JMeter was used to do load testing on the endpoint with a variation
between 5 - 15 requests/sec. The JMeter script used is inside the project
along with a report of the executed test.