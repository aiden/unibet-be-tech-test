using System;
using System.Threading;
using System.Threading.Tasks;

namespace ExchangeRates.WebApi.Utils
{
    public class RetryUtil
    {
        public class RetryMaxReachedException : Exception
        {
            public RetryMaxReachedException(string message) : base(message)
            {
            }
        }

        public static async Task<T> RetryAsync<T>(
            int maxRetries,
            TimeSpan delayBetweenRetries,
            CancellationToken cancellationToken,
            Func<int, Task<T>> runner,
            Action<Exception> onException)
        {
            for (var i = 0; i < maxRetries; i++)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    break;
                }
                // wait if retrying
                if (i > 0)
                {
                    await Task.Delay(delayBetweenRetries, cancellationToken);
                }
                // execute runner
                try
                {
                    return await runner(i);
                }
                catch (TaskCanceledException)
                {
                    // continue
                }
                catch (Exception e)
                {
                    onException(e);
                }
            }
            // if here then throw exception
            // maximum retries
            throw new RetryMaxReachedException("Max retries reached");
        }
    }
}