using System;
using ExchangeRates.WebApi.Models;

namespace ExchangeRates.WebApi.Exceptions
{
    public class FixerNonSuccessfulResponseException : Exception
    {
        public FixerGetLatestResponse Response { get; }

        public FixerNonSuccessfulResponseException(FixerGetLatestResponse response, string message) : base(message)
        {
            Response = response;
        }
    }
}