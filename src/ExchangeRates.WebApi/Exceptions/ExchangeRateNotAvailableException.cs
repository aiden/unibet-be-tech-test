using System;

namespace ExchangeRates.WebApi.Exceptions
{
    public class ExchangeRateNotAvailableException : Exception
    {
        public string BaseCurrency { get; }
        public string TargetCurrency { get; }

        public ExchangeRateNotAvailableException(string baseCurrency, string targetCurrency, string message) :
            base(message)
        {
            BaseCurrency = baseCurrency;
            TargetCurrency = targetCurrency;
        }
    }
}