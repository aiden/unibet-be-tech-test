using System;

namespace ExchangeRates.WebApi.Exceptions
{
    public class ExchangeRateUpdateException : Exception
    {
        public string BaseCurrency { get; }

        public ExchangeRateUpdateException(string baseCurrency, string message) : base(message)
        {
            BaseCurrency = baseCurrency;
        }
    }
}