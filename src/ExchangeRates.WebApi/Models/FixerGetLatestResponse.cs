using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ExchangeRates.WebApi.Models
{
    /**
     * Response from fixer api to get latest currency rates
     * Example url http://data.fixer.io/api/latest?access_key=a690f421ff4b3f125c392faaded3eb1f&base=EUR&symbols=AUD,SEK,USD,GBP,EUR
     */
    public class FixerGetLatestResponse
    {
        public bool Success { get; set; }

        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime Timestamp { get; set; }

        public string Base { get; set; } = "";
        public IDictionary<string, decimal> Rates { get; set; } = new Dictionary<string, decimal>();
    }
}