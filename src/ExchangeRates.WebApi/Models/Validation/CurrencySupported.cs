using System.ComponentModel.DataAnnotations;
using ExchangeRates.WebApi.Config;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace ExchangeRates.WebApi.Models.Validation
{
    public class CurrencySupported : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var options = validationContext.GetService<IOptions<ExchangeRateConfig>>();
            var currency = (string) value;
            var exists = options.Value.Currencies.Contains(currency);
            return exists
                ? ValidationResult.Success
                : new ValidationResult(
                    $"Unsupported currency please choose a supported currency ({string.Join(",", options.Value.Currencies)})");
        }
    }
}