using System;
using Newtonsoft.Json;

namespace ExchangeRates.WebApi.Models
{
    public class ExchangeRate
    {
        [JsonIgnore]
        public string Id => $"{BaseCurrency}_{TargetCurrency}";
        public string BaseCurrency { get; set; }
        public string TargetCurrency { get; set; }
        private decimal _rate;

        [JsonIgnore]
        public decimal Rate
        {
            get => _rate;
            set
            {
                _rate = value;
                // truncate rate for json response
                // to limit it to x number of decimal places
                const int decimalPlaces = 5;
                var power = (decimal) Math.Pow(10.0, decimalPlaces);
                RateTruncated = Math.Truncate(power * Rate) / power;
            }
        }

        [JsonProperty(PropertyName = "exchangeRate")]
        public decimal RateTruncated { get; set; }
        public DateTime Timestamp { get; set; }

        public ExchangeRate GetInversion()
        {
            return new ExchangeRate
            {
                Rate = 1m / Rate,
                BaseCurrency = TargetCurrency,
                TargetCurrency = BaseCurrency,
                Timestamp = Timestamp
            };
        }
    }
}