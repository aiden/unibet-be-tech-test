using System.ComponentModel.DataAnnotations;
using ExchangeRates.WebApi.Models.Validation;

namespace ExchangeRates.WebApi.Models
{
    public class RetrieveExchangeRateRequest
    {
        private string _baseCurrency;
        private string _targetCurrency;

        [Required]
        [CurrencySupported]
        public string BaseCurrency
        {
            get => _baseCurrency;
            set => _baseCurrency = value.ToUpper();
        }

        [Required]
        [CurrencySupported]
        public string TargetCurrency
        {
            get => _targetCurrency;
            set => _targetCurrency = value.ToUpper();
        }
    }
}