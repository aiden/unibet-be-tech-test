﻿using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Models;
using ExchangeRates.WebApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace ExchangeRates.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RetrieveExchangeRateController : ControllerBase
    {
        private readonly IExchangeRateService _exchangeRateService;

        public RetrieveExchangeRateController(IExchangeRateService exchangeRateService)
        {
            _exchangeRateService = exchangeRateService;
        }

        // GET api/RetrieveExchangeRate?baseCurrency=EUR&targetCurrency=AUD
        [HttpGet]
        public async Task<ActionResult<ExchangeRate>> GetAsync([FromQuery] RetrieveExchangeRateRequest body,
            CancellationToken cancellationToken)
        {
            var result =
                await _exchangeRateService.GetExchangeRateWhenAvailableAsync(body.BaseCurrency, body.TargetCurrency,
                    cancellationToken);
            return Ok(result);
        }

        // GET api/RetrieveExchangeRate/list
        [HttpGet("list")]
        public async Task<ActionResult<IImmutableSet<ExchangeRate>>> ListAsync(CancellationToken cancellationToken)
        {
            var result = await _exchangeRateService.GetAllExchangeRatesAsync(cancellationToken);
            return Ok(result);
        }
    }
}