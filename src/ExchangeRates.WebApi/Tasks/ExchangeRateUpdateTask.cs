using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Config;
using ExchangeRates.WebApi.Exceptions;
using ExchangeRates.WebApi.Models;
using ExchangeRates.WebApi.Services;
using ExchangeRates.WebApi.Utils;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StackExchange.Redis.Extensions.Core;

namespace ExchangeRates.WebApi.Tasks
{
    public class ExchangeRateUpdateTask : IExchangeRateUpdateTask
    {
        private readonly IFixerApiService _fixerApiService;
        private readonly IExchangeRateService _exchangeRateService;
        private readonly IOptions<ExchangeRateConfig> _config;
        private readonly ILogger<ExchangeRateUpdateTask> _logger;
        private readonly ICacheClient _redis;
        private string _baseCurrency;
        public TimeSpan DelayBetweenRetries { get; set; } = TimeSpan.FromSeconds(1);

        public ExchangeRateUpdateTask(IFixerApiService fixerApiService,
            IExchangeRateService exchangeRateService,
            IOptions<ExchangeRateConfig> config,
            ICacheClient redis,
            ILogger<ExchangeRateUpdateTask> logger)
        {
            _exchangeRateService = exchangeRateService;
            _fixerApiService = fixerApiService;
            _config = config;
            _redis = redis;
            _logger = logger;
        }

        public IExchangeRateUpdateTask WithBaseCurrency(string baseCurrency)
        {
            _baseCurrency = baseCurrency;
            return this;
        }

        public Func<CancellationToken, Task<FixerGetLatestResponse>> Run()
        {
            var baseCurrency = _baseCurrency ?? throw new ArgumentNullException(nameof(_baseCurrency));
            // convert target currencies into immutable set
            var baseCurrencies = ImmutableSortedSet.Create(_config.Value.BaseCurrencies.OrderBy(_ => _).ToArray());
            var targetCurrencies = ImmutableSortedSet.Create(_config.Value.Currencies.OrderBy(_ => _).ToArray());
            var baseIndex = baseCurrencies.IndexOf(baseCurrency);

            return async cancellationToken =>
            {
                var maxFetchRetries = _config.Value.Fixer.MaxFetchRetries;
                try
                {
                    return await RetryUtil.RetryAsync(maxFetchRetries,
                        DelayBetweenRetries,
                        cancellationToken,
                        TryGetAsync(baseCurrency, targetCurrencies, cancellationToken, baseIndex, baseCurrencies),
                        ex =>
                        {
                            _logger.LogError($"Failed to fetch currency rates for BaseCurrency:{baseCurrency}", ex);
                        });

                }
                catch (Exception e)
                {
                    _logger.LogError($"Failed to fetch currency rates for BaseCurrency:{baseCurrency}", e);
                    throw new ExchangeRateUpdateException(baseCurrency, $"Failed to fetch currency rates for BaseCurrency:{baseCurrency}");
                }

            };
        }

        private Func<int, Task<FixerGetLatestResponse>> TryGetAsync(string baseCurrency,
            IImmutableSet<string> targetCurrencies,
            CancellationToken cancellationToken,
            int baseIndex,
            IImmutableSet<string> baseCurrencies)
        {
            return async retryCount =>
            {
                _logger.LogInformation(
                    $"Begin fetching currency rates for Attempt:{retryCount}, BaseCurrency:{baseCurrency}");
                var timestampKey = $"ExchangeRateUpdateTask:{baseCurrency}";
                var prevResult = await _redis.GetAsync<FixerGetLatestResponse>(timestampKey);
                if (prevResult != null)
                {
                    _logger.LogInformation($"Skipping re fetch on BaseCurrency:{baseCurrency}");
                    return prevResult;
                }

                var result = await _fixerApiService.GetLatestAsync(baseCurrency, targetCurrencies,
                    cancellationToken);
                _logger.LogInformation(
                    $"Received results {result.Success} {result.Timestamp} {result.Base} {result.Rates}");
                if (!result.Success)
                {
                    return await Task.FromCanceled<FixerGetLatestResponse>(new CancellationToken(true));
                }

                _logger.LogInformation(
                    $"Successfully fetched currency rates for Attempt:{retryCount}, BaseCurrency:{baseCurrency}");

                // halve the expiry since our timer
                // will only update every ExpireDurationSeconds
                // and this will ensure object definitely does
                // not exist in the cache
                var expireTime =
                    TimeSpan.FromSeconds(Math.Max(1, _config.Value.Fixer.ExpireDurationSeconds / 2));
                // store update in redis
                await _redis.AddAsync(timestampKey, result, expireTime);

                // create all exchange rates
                var rates = GetExchangeRatesFromResult(result, baseCurrency, baseIndex > 0, baseCurrencies,
                    targetCurrencies);

                // save all
                await _exchangeRateService.SaveExchangeRate(rates.ToArray());
                return result;
            };
        }

        private static IEnumerable<ExchangeRate> GetExchangeRatesFromResult(
            FixerGetLatestResponse result,
            string baseCurrency,
            bool shouldApproximate,
            IImmutableSet<string> baseCurrencies,
            IEnumerable<string> targetCurrencies)
        {
            // for the current base against all rates
            var baseToCurrencies = result.Rates.Select(CreateBaseExchangeRates(baseCurrency, result.Timestamp));

            // calculate the inverse by the algorithm
            // RateA -> Base = 1 / (Base -> RateA)
            var baseToCurrenciesInv = CreateExchangeRateInversions(baseToCurrencies, baseCurrencies);

            // next check if there are any other base currencies
            // that are being used by the rates and if so do nothing
            // otherwise we need to convert from  RateA -> Base -> RateB
            // the algorithm for this looks like
            // RateA -> RateB = (Base -> RateB) / (Base -> RateA)
            var currenciesToCurrencies = shouldApproximate
                ? new List<ExchangeRate>()
                : CreateApproximateExchangeRates(result.Rates, result.Timestamp, baseCurrencies, targetCurrencies);

            // combine all rates
            var allCurrencies = baseToCurrencies
                .Concat(baseToCurrenciesInv)
                .Concat(currenciesToCurrencies);
            return allCurrencies;
        }

        private static Func<KeyValuePair<string, decimal>, ExchangeRate> CreateBaseExchangeRates(
            string baseCurrency,
            DateTime timestamp)
        {
            return kv => new ExchangeRate
            {
                Rate = kv.Value,
                BaseCurrency = baseCurrency,
                TargetCurrency = kv.Key.ToUpper(),
                Timestamp = timestamp
            };
        }

        private static IEnumerable<ExchangeRate> CreateExchangeRateInversions(
            IEnumerable<ExchangeRate> exchangeRates,
            IImmutableSet<string> baseCurrencies)
        {
            return exchangeRates
                .Where(v => !baseCurrencies.Contains(v.TargetCurrency))
                .Select(r => r.GetInversion());
        }

        private static IEnumerable<ExchangeRate> CreateApproximateExchangeRates(
            IDictionary<string, decimal> rates,
            DateTime timestamp,
            IEnumerable<string> baseCurrencies,
            IEnumerable<string> targetCurrencies)
        {
            return rates
                .Where(kv => !baseCurrencies.Contains(kv.Key))
                .SelectMany(kv =>
                {
                    // find all other currencies which
                    // are not the current target or base
                    var (key, value) = kv;
                    return targetCurrencies
                        .Where(c => !(c.Equals(key) || baseCurrencies.Contains(c)))
                        .Select(c => new ExchangeRate
                            {
                                Rate = rates[c] / value,
                                BaseCurrency = key.ToUpper(),
                                TargetCurrency = c,
                                Timestamp = timestamp
                            }
                        );
                });
        }
    }
}