using System;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Models;

namespace ExchangeRates.WebApi.Tasks
{
    public interface IExchangeRateUpdateTask
    {
        IExchangeRateUpdateTask WithBaseCurrency(string baseCurrency);
        Func<CancellationToken, Task<FixerGetLatestResponse>> Run();
    }
}