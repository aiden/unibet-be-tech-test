using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DalSoft.Hosting.BackgroundQueue;
using ExchangeRates.WebApi.Config;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ExchangeRates.WebApi.Tasks
{
    public class ExchangeRateUpdateHostedService : IHostedService, IDisposable
    {
        private readonly ILogger<ExchangeRateUpdateHostedService> _logger;
        private readonly IOptions<ExchangeRateConfig> _config;
        private readonly BackgroundQueue _backgroundQueue;
        private readonly IServiceProvider _services;
        private Timer _updateTimer;

        public ExchangeRateUpdateHostedService(ILogger<ExchangeRateUpdateHostedService> logger,
            IOptions<ExchangeRateConfig> config,
            BackgroundQueue backgroundQueue,
            IServiceProvider services)
        {
            _logger = logger;
            _config = config;
            _backgroundQueue = backgroundQueue;
            _services = services;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Exchange Rate Update Service starting");
            _updateTimer = new Timer(OnUpdate, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(_config.Value.Fixer.ExpireDurationSeconds));
            return Task.CompletedTask;
        }

        protected virtual IExchangeRateUpdateTask CreateUpdateTask()
        {
            return _services
                .CreateScope()
                .ServiceProvider
                .GetRequiredService<IExchangeRateUpdateTask>();
        }

        public void OnUpdate(object state)
        {
            _logger.LogInformation("Begin updating exchange rates");
            // loop through base currencies and create a background
            // task to fetch the data from fixer api
            _config.Value.BaseCurrencies
                .Select(baseCurrency =>
                    CreateUpdateTask()
                        .WithBaseCurrency(baseCurrency)
                        .Run())
                .ToList()
                .ForEach(_backgroundQueue.Enqueue);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Exchange Rate Update Service stopping");
            _updateTimer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _updateTimer?.Dispose();
        }
    }
}