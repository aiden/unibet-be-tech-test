using System.Collections.Generic;

namespace ExchangeRates.WebApi.Config
{
    public class ExchangeRateConfig
    {
        public class FixerConfig
        {
            public string ApiKey { get; set; } = "";

            public string GetLatestEndPoint { get; set; } = "";

            /**
             * Amount of time in seconds before re fetching
             * exchange rates.
             */
            public int ExpireDurationSeconds { get; set; } = 60;
            public int MaxFetchRetries { get; set; } = 5;
        }

        public FixerConfig Fixer { get; set; } = new FixerConfig();
        public int MaxWaitForRateRetries { get; set; } = 5;
        public HashSet<string> BaseCurrencies { get; set; } = new HashSet<string>();
        public HashSet<string> Currencies { get; set; } = new HashSet<string>();
    }
}