using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Models;

namespace ExchangeRates.WebApi.Services
{
    public interface IFixerApiService
    {
        /**
         * Asynchronously loads latest currency rates for the given base currency and
         * target currencies.
         * @throws FixerNonSuccessfulResponseException if response from fixer has Success == false
         */
        Task<FixerGetLatestResponse> GetLatestAsync(string baseCurrency, IImmutableSet<string> targetCurrencies,
            CancellationToken cancellationToken);
    }
}