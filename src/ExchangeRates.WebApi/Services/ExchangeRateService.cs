using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Config;
using ExchangeRates.WebApi.Exceptions;
using ExchangeRates.WebApi.Models;
using ExchangeRates.WebApi.Utils;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StackExchange.Redis.Extensions.Core;

namespace ExchangeRates.WebApi.Services
{
    public class ExchangeRateService : IExchangeRateService
    {
        private readonly IOptions<ExchangeRateConfig> _config;
        private readonly ILogger<ExchangeRateService> _logger;
        private readonly ICacheClient _redis;
        public TimeSpan DelayBetweenRetries { get; set; } = TimeSpan.FromSeconds(1);

        public ExchangeRateService(ILogger<ExchangeRateService> logger,
            IOptions<ExchangeRateConfig> config,
            ICacheClient redis)
        {
            _logger = logger;
            _config = config;
            _redis = redis;
        }

        public async Task<IImmutableSet<ExchangeRate>> GetAllExchangeRatesAsync(CancellationToken cancellationToken)
        {
            var allKeys = await _redis.SearchKeysAsync("ExchangeRate:*");
            var result = await _redis.GetAllAsync<ExchangeRate>(allKeys);
            return result.Values
                .ToImmutableSortedSet(Comparer<ExchangeRate>.Create((o1, o2) => string.CompareOrdinal(o1.Id, o2.Id)));
        }

        public async Task<ExchangeRate> GetExchangeRateAsync(string baseCurrency, string targetCurrency,
            CancellationToken cancellationToken)
        {
            // maybe doing something silly like
            // passing in base and target as the same?
            if (baseCurrency.ToUpper().Equals(targetCurrency.ToUpper()))
            {
                return new ExchangeRate
                {
                    Rate = 1.0m,
                    Timestamp = DateTime.Now,
                    BaseCurrency = baseCurrency.ToUpper(),
                    TargetCurrency = targetCurrency.ToUpper()
                };
            }
            // attempt to find in cache
            var result = await _redis.GetAsync<ExchangeRate>(
                $"ExchangeRate:{baseCurrency.ToUpper()}_{targetCurrency.ToUpper()}");
            if (result == null)
            {
                throw new ExchangeRateNotAvailableException(baseCurrency, targetCurrency,
                    $"Exchange rate not available to convert from {baseCurrency} to {targetCurrency}");
            }

            return result;
        }

        public async Task<ExchangeRate> GetExchangeRateWhenAvailableAsync(string baseCurrency, string targetCurrency,
            CancellationToken cancellationToken)
        {
            var maxWaitForRateRetries = _config.Value.MaxWaitForRateRetries;
            try
            {
                return await RetryUtil.RetryAsync(maxWaitForRateRetries,
                    DelayBetweenRetries,
                    cancellationToken,
                    async retryCount =>
                    {
                        _logger.LogInformation(
                            $"Get exchange rate when available begin Attempt:{retryCount}, BaseCurrency:{baseCurrency}, TargetCurrency:{targetCurrency}");
                        var result = await GetExchangeRateAsync(baseCurrency, targetCurrency, cancellationToken);
                        _logger.LogInformation(
                            $"Get exchange rate when available success Attempt:{retryCount}, BaseCurrency:{baseCurrency}, TargetCurrency:{targetCurrency}");
                        return result;
                    }, ex =>
                    {
                        _logger.LogError(
                            $"Get exchange rate when available failed on BaseCurrency:{baseCurrency}, TargetCurrency:{targetCurrency}",
                            ex);
                    });

            }
            catch (Exception)
            {
                throw new ExchangeRateNotAvailableException(baseCurrency, targetCurrency,
                    $"Exchange rate not available to convert from {baseCurrency} to {targetCurrency}");
            }
        }

        public Task SaveExchangeRate(params ExchangeRate[] rates)
        {
            return _redis.AddAllAsync(rates
                .Select(r => new Tuple<string, ExchangeRate>($"ExchangeRate:{r.Id}", r))
                .ToList()
            );
        }
    }
}