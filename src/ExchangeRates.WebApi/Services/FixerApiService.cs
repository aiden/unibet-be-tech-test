using System.Collections.Immutable;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Config;
using ExchangeRates.WebApi.Exceptions;
using ExchangeRates.WebApi.Models;
using Microsoft.Extensions.Options;

namespace ExchangeRates.WebApi.Services
{
    public class FixerApiService : IFixerApiService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IOptions<ExchangeRateConfig> _config;

        public FixerApiService(IHttpClientFactory httpClientFactory, IOptions<ExchangeRateConfig> config)
        {
            _httpClientFactory = httpClientFactory;
            _config = config;
        }

        public async Task<FixerGetLatestResponse> GetLatestAsync(string baseCurrency, IImmutableSet<string> targetCurrencies,
            CancellationToken cancellationToken)
        {
            var fixerConfig = _config.Value.Fixer;
            // no need to request base currency in target currencies since always == 1
            var symbols = string.Join(",", targetCurrencies
                .Remove(baseCurrency)
                .ToList()
                .OrderBy(s => s)
            );
            var request = new HttpRequestMessage(HttpMethod.Get,
                $"{fixerConfig.GetLatestEndPoint}?access_key={fixerConfig.ApiKey}&base={baseCurrency}&symbols={symbols}");
            request.Headers.Add("Accept", "application/json");

            var client = _httpClientFactory.CreateClient();
            var response = await client.SendAsync(request, cancellationToken);
            // throw exception if invalid request
            response.EnsureSuccessStatusCode();
            // convert response into object
            var res = await response.Content.ReadAsAsync<FixerGetLatestResponse>(cancellationToken);
            // if unsuccessful then
            // throw exception
            if (!res.Success)
            {
                throw new FixerNonSuccessfulResponseException(res, "Fixer response failed with Success == false");
            }
            return res;
        }
    }
}