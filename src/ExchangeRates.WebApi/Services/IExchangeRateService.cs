using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Models;

namespace ExchangeRates.WebApi.Services
{
    public interface IExchangeRateService
    {
        /**
         * Asynchronously retrieves all exchange rates
         */
        Task<IImmutableSet<ExchangeRate>> GetAllExchangeRatesAsync(CancellationToken cancellationToken);

        /**
         * Asynchronously retrieves the exchange rate from the given
         * base currency and target currency
         */
        Task<ExchangeRate> GetExchangeRateAsync(string baseCurrency, string targetCurrency,
            CancellationToken cancellationToken);

        /**
         * Asynchronously waits for the exchange rate to be available.
         * If available immediately then will result immediately
         * otherwise will continue to wait for rate availability.
         */
        Task<ExchangeRate> GetExchangeRateWhenAvailableAsync(string baseCurrency, string targetCurrency,
            CancellationToken cancellationToken);

        /**
         * Asynchronously saves multiple exchange rates into
         * the database
         */
        Task SaveExchangeRate(params ExchangeRate[] rates);
    }
}