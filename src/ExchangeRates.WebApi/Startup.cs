﻿using DalSoft.Hosting.BackgroundQueue.DependencyInjection;
using ExchangeRates.WebApi.Config;
using ExchangeRates.WebApi.Middleware;
using ExchangeRates.WebApi.Services;
using ExchangeRates.WebApi.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Core.Configuration;
using StackExchange.Redis.Extensions.Newtonsoft;

namespace ExchangeRates.WebApi
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<Startup> _logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var redisConfiguration = _configuration.GetSection("Redis").Get<RedisConfiguration>();
            services
                .AddOptions()
                .Configure<ExchangeRateConfig>(_configuration.GetSection("ExchangeRateConfig"))
                .AddSingleton<IFixerApiService, FixerApiService>()
                .AddSingleton<IExchangeRateService, ExchangeRateService>()
                .AddHostedService<ExchangeRateUpdateHostedService>()
                .AddScoped<IExchangeRateUpdateTask, ExchangeRateUpdateTask>()
                .AddSingleton(redisConfiguration)
                .AddSingleton<ICacheClient, StackExchangeRedisCacheClient>()
                .AddSingleton<ISerializer, NewtonsoftSerializer>()
                .AddHttpClient()
                .AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddBackgroundQueue(exception => _logger.LogError("Background task failed", exception), 5);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMiddleware<ExceptionHandlingMiddleware>();
            app.UseMvc();
        }
    }
}