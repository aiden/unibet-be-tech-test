using System;
using System.Net;
using ExchangeRates.WebApi.Exceptions;
using ExchangeRates.WebApi.Middleware;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using NUnit.Framework;

namespace ExchangeRates.Tests.Middleware
{
    public class ExceptionHandlingMiddlewareTest
    {
        [Test]
        [TestCase(TestName = "Handles unknown exception as 500")]
        public void Handle_Unknown()
        {
            var ctxMock = new DefaultHttpContext();
            var resMock = new DefaultHttpResponse(ctxMock);
            ExceptionHandlingMiddleware.HandleExceptionAsync(ctxMock, new Exception("something"));
            Assert.AreEqual("application/json", resMock.ContentType);
            Assert.AreEqual((int) HttpStatusCode.InternalServerError, resMock.StatusCode);
        }

        [Test]
        [TestCase(TestName = "Handles ExchangeRateNotAvailableException as 404")]
        public void Handle_ExchangeRateNotAvailableException()
        {
            var ctxMock = new DefaultHttpContext();
            var resMock = new DefaultHttpResponse(ctxMock);
            ExceptionHandlingMiddleware.HandleExceptionAsync(ctxMock,
                new ExchangeRateNotAvailableException("AUD", "SEK", "something"));
            Assert.AreEqual("application/json", resMock.ContentType);
            Assert.AreEqual((int) HttpStatusCode.NotFound, resMock.StatusCode);
        }
    }
}