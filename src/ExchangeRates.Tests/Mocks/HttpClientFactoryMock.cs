using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;

namespace ExchangeRates.Tests.Mocks
{
    public class HttpClientFactoryMock
    {
        public Mock<IHttpClientFactory> MockHttpClient;
        private Mock<HttpMessageHandler> _mockMessageHandler;

        private HttpClientFactoryMock Setup(string responseContent, HttpStatusCode statusCode)
        {
            MockHttpClient = new Mock<IHttpClientFactory>();
            _mockMessageHandler = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            _mockMessageHandler
                .Protected()
                // Setup the PROTECTED method to mock
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                // prepare the expected response of the mocked http call
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = statusCode,
                    Content = new StringContent(responseContent, Encoding.UTF8, "application/json")
                })
                .Verifiable();

            // use real http client with mocked handler here
            var httpClient = new HttpClient(_mockMessageHandler.Object);

            MockHttpClient.Setup(f => f.CreateClient(It.IsAny<string>())).Returns(httpClient);
            return this;
        }

        public HttpClientFactoryMock SetupSuccess(string responseContent)
        {
            return Setup(responseContent, HttpStatusCode.OK);
        }

        public HttpClientFactoryMock SetupFailure(string responseContent,
            HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return Setup(responseContent, statusCode);
        }

        public HttpClientFactoryMock VerifySendAsync(
            string requestUri,
            Times times)
        {
            var expectedUri = new Uri(requestUri);
            _mockMessageHandler
                .Protected()
                .Verify("SendAsync", times,
                    ItExpr.Is<HttpRequestMessage>(req =>
                        req.Method == HttpMethod.Get &&
                        req.RequestUri.Equals(expectedUri)),
                    ItExpr.IsAny<CancellationToken>());

            return this;
        }
    }
}