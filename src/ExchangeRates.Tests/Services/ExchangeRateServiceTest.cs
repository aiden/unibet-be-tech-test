using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Config;
using ExchangeRates.WebApi.Exceptions;
using ExchangeRates.WebApi.Models;
using ExchangeRates.WebApi.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;

namespace ExchangeRates.Tests.Services
{
    public class ExchangeRateServiceTest
    {
        private Mock<IOptions<ExchangeRateConfig>> _configMock;
        private Mock<ILogger<ExchangeRateService>> _loggerMock;
        private Mock<ICacheClient> _redisMock;
        private ExchangeRateService _service;
        private ExchangeRate _rate;
        private CancellationToken _cancellationToken;

        [SetUp]
        public void SetupTests()
        {
            var config = new ExchangeRateConfig {MaxWaitForRateRetries = 1};
            _configMock = new Mock<IOptions<ExchangeRateConfig>>();
            _loggerMock = new Mock<ILogger<ExchangeRateService>>();
            _redisMock = new Mock<ICacheClient>();
            _cancellationToken = new CancellationToken();
            _service = new ExchangeRateService(_loggerMock.Object, _configMock.Object, _redisMock.Object){DelayBetweenRetries = TimeSpan.Zero};
            _configMock.Setup(_ => _.Value).Returns(config);

            _rate = new ExchangeRate
            {
                BaseCurrency = "A",
                TargetCurrency = "B",
                Rate = 1.1m,
                Timestamp = DateTime.UtcNow
            };
        }

        [Test]
        [TestCase(TestName = "Get exchange rate when exists")]
        public async Task GetExchangeRate_When_Exists()
        {
            _redisMock.Setup(_ => _.GetAsync<ExchangeRate>("ExchangeRate:A_B", It.IsAny<CommandFlags>()))
                .ReturnsAsync(_rate);
            var result = await _service.GetExchangeRateAsync("A", "B", _cancellationToken);
            Assert.AreSame(_rate, result);
        }

        [Test]
        [TestCase(TestName = "Get exchange rate when baseCurrency == targetCurrency")]
        public async Task GetExchangeRate_When_Base_Equals_Target()
        {
            _redisMock.Verify(_ => _.GetAsync<ExchangeRate>(It.IsAny<string>(), It.IsAny<CommandFlags>()), Times.Never);
            var result = await _service.GetExchangeRateAsync("A", "A", _cancellationToken);
            Assert.AreEqual(1.0m, result.Rate);
            Assert.AreEqual("A_A", result.Id);
        }

        [Test]
        [TestCase(TestName =
            "Get exchange rate when does not exist should throw exchange rate not available exception")]
        public void GetExchangeRate_When_Not_Exists()
        {
            _redisMock.Setup(_ => _.GetAsync<ExchangeRate>("ExchangeRate:A_B", It.IsAny<CommandFlags>()))
                .ReturnsAsync((ExchangeRate) null);
            Assert.ThrowsAsync<ExchangeRateNotAvailableException>(() =>
                _service.GetExchangeRateAsync("A", "B", _cancellationToken));
        }

        [Test]
        [TestCase(TestName = "Get exchange rate when available when exists")]
        public async Task GetExchangeRate_When_Available_And_Exists()
        {
            _redisMock.Setup(_ => _.GetAsync<ExchangeRate>("ExchangeRate:A_B", It.IsAny<CommandFlags>()))
                .ReturnsAsync(_rate);
            var result = await _service.GetExchangeRateWhenAvailableAsync("A", "B", _cancellationToken);
            Assert.AreSame(_rate, result);
        }

        [Test]
        [TestCase(TestName =
            "Get exchange rate when available should wait n number of times before throw exchange rate not available exception")]
        public void GetExchangeRate_When_Available()
        {
            _redisMock.Setup(_ => _.GetAsync<ExchangeRate>("ExchangeRate:A_B", It.IsAny<CommandFlags>()));
            Assert.ThrowsAsync<ExchangeRateNotAvailableException>(() =>
                _service.GetExchangeRateWhenAvailableAsync("A", "B", _cancellationToken));
        }

        [Test]
        [TestCase(TestName = "Save exchange rates")]
        public async Task SaveExchangeRates()
        {
            var rate1 = new ExchangeRate {BaseCurrency = "A", TargetCurrency = "B"};
            var rate2 = new ExchangeRate {BaseCurrency = "B", TargetCurrency = "C"};
            var dict = new Dictionary<string, ExchangeRate>
            {
                {"ExchangeRate:A_B", rate1},
                {"ExchangeRate:B_C", rate2}
            };
            await _service.SaveExchangeRate(rate1, rate2);
            _redisMock.Verify(_ => _.AddAllAsync(It.Is<IList<Tuple<string, ExchangeRate>>>(items =>
                dict.All(kv => items.First(t => t.Item1.Equals(kv.Key) && t.Item2 == kv.Value) != null))));
        }

        [Test]
        [TestCase(TestName = "List exchange rates")]
        public async Task ListExchangeRates()
        {
            var rate1 = new ExchangeRate {BaseCurrency = "A", TargetCurrency = "B"};
            var rate2 = new ExchangeRate {BaseCurrency = "B", TargetCurrency = "C"};
            var list = new Dictionary<string, ExchangeRate>
            {
                {"A", rate1},
                {"B", rate2}
            };
            _redisMock.Setup(_ => _.SearchKeysAsync(It.IsAny<string>())).ReturnsAsync(new[] {"A", "B"});
            _redisMock.Setup(_ => _.GetAllAsync<ExchangeRate>(It.IsAny<IEnumerable<string>>())).ReturnsAsync(list);
            var result = await _service.GetAllExchangeRatesAsync(_cancellationToken);
            Assert.AreEqual(2, result.Count);
        }
    }
}