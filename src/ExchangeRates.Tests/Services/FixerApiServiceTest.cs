using System;
using System.Collections.Immutable;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.Tests.Mocks;
using ExchangeRates.WebApi.Config;
using ExchangeRates.WebApi.Exceptions;
using ExchangeRates.WebApi.Models;
using ExchangeRates.WebApi.Services;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace ExchangeRates.Tests.Services
{
    public class FixerApiServiceTest
    {
        private FixerApiService _service;
        private HttpClientFactoryMock _mockHttpClientFactory;
        private Mock<IOptions<ExchangeRateConfig>> _mockConfig;


        [SetUp]
        public void SetupTests()
        {
            var config = new ExchangeRateConfig
            {
                Fixer = {ApiKey = "abc", GetLatestEndPoint = "http://test.com/api/test", ExpireDurationSeconds = 10}
            };

            _mockHttpClientFactory = new HttpClientFactoryMock();

            _mockConfig = new Mock<IOptions<ExchangeRateConfig>>();
            _mockConfig.Setup(f => f.Value).Returns(config);
        }

        [Test]
        [TestCase(TestName = "Get latest fetches currency rates")]
        public async Task GetLatestSuccess()
        {
            var response = new FixerGetLatestResponse {
                Success = true,
                Base = "EUR",
                Timestamp = DateTime.Parse("2019-01-02T00:00:00").ToUniversalTime(),
                Rates = {
                    {"AUD", 1.5m},
                    {"GBP", 1.23456m}
                }
            };
            _mockHttpClientFactory.SetupSuccess(JsonConvert.SerializeObject(response));

            _service = new FixerApiService(_mockHttpClientFactory.MockHttpClient.Object, _mockConfig.Object);
            var cancellationToken = new CancellationToken(false);

            var result = await _service.GetLatestAsync("A", ImmutableHashSet.Create("A", "B", "C"), cancellationToken);
            Assert.AreEqual(result.Base, response.Base);
            Assert.AreEqual(result.Timestamp, response.Timestamp);
            Assert.AreEqual(result.Success, response.Success);
            Assert.AreEqual(result.Rates.Keys, response.Rates.Keys);

            _mockHttpClientFactory.VerifySendAsync("http://test.com/api/test?access_key=abc&base=A&symbols=B,C",
                Times.Exactly(1));
        }

        [Test]
        [TestCase(TestName = "Get latest fetches currency rates with Success = false")]
        public void GetLatestSuccessEqualsFalse()
        {
            var response = new FixerGetLatestResponse {
                Success = false,
                Base = "EUR",
                Timestamp = DateTime.Parse("2019-01-02T00:00:00").ToUniversalTime()
            };
            _mockHttpClientFactory.SetupSuccess(JsonConvert.SerializeObject(response));

            _service = new FixerApiService(_mockHttpClientFactory.MockHttpClient.Object, _mockConfig.Object);
            var cancellationToken = new CancellationToken(false);

            Assert.ThrowsAsync<FixerNonSuccessfulResponseException>(async () =>
                await _service.GetLatestAsync("A", ImmutableHashSet.Create("A", "B", "C"), cancellationToken));

            _mockHttpClientFactory.VerifySendAsync("http://test.com/api/test?access_key=abc&base=A&symbols=B,C",
                Times.Exactly(1));
        }

        [Test]
        [TestCase(TestName = "Get latest fails to fetch currency rates")]
        public void GetLatestFailure()
        {
            _mockHttpClientFactory.SetupFailure("Invalid response");

            _service = new FixerApiService(_mockHttpClientFactory.MockHttpClient.Object, _mockConfig.Object);
            var cancellationToken = new CancellationToken(false);

            Assert.ThrowsAsync<HttpRequestException>(async () =>
                await _service.GetLatestAsync("A", ImmutableHashSet.Create("B", "C"), cancellationToken));
        }
    }
}