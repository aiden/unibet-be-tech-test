using System.Collections.Immutable;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Controllers;
using ExchangeRates.WebApi.Models;
using ExchangeRates.WebApi.Services;
using Moq;
using NUnit.Framework;

namespace ExchangeRates.Tests.Controllers
{
    public class RetrieveExchangeRateControllerTest
    {
        private Mock<IExchangeRateService> _mockService;
        private RetrieveExchangeRateController _controller;
        private CancellationToken _cancellationToken;

        [SetUp]
        public void Setup()
        {
            _mockService = new Mock<IExchangeRateService>();
            _cancellationToken = new CancellationToken();
            _controller = new RetrieveExchangeRateController(_mockService.Object);
        }

        [Test]
        [TestCase(TestName = "Gets exchange rate with base and target currency")]
        public async Task Gets_Exchange_Rate()
        {
            var req = new RetrieveExchangeRateRequest
            {
                BaseCurrency = "A",
                TargetCurrency = "B"
            };
            var rate = new ExchangeRate();
            _mockService.Setup(_ => _.GetExchangeRateWhenAvailableAsync("A", "B", It.IsAny<CancellationToken>()))
                .ReturnsAsync(rate);
            await _controller.GetAsync(req, _cancellationToken);
            _mockService.Verify(_ => _.GetExchangeRateWhenAvailableAsync("A", "B", It.IsAny<CancellationToken>()),
                Times.Exactly(1));
        }

        [Test]
        [TestCase(TestName = "Gets all exchange rates")]
        public async Task Gets_All_Exchange_Rates()
        {
            var rates = ImmutableHashSet.Create<ExchangeRate>();
            _mockService.Setup(_ => _.GetAllExchangeRatesAsync(It.IsAny<CancellationToken>())).ReturnsAsync(rates);
            await _controller.ListAsync(_cancellationToken);
            _mockService.Verify(_ => _.GetAllExchangeRatesAsync(It.IsAny<CancellationToken>()), Times.Exactly(1));
        }
    }
}