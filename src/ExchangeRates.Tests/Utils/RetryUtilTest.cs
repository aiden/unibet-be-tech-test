using System;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Utils;
using Moq;
using NUnit.Framework;

namespace ExchangeRates.Tests.Utils
{
    public class RetryUtilTest
    {

        [Test]
        [TestCase(TestName = "executes result")]
        public async Task ExecutesResult()
        {
            var mockFn = new Mock<Func<int, Task<bool>>>();
            mockFn.Setup(_ => _(It.IsAny<int>())).ReturnsAsync(true);
            var mockEx = new Mock<Action<Exception>>();
            var result = await RetryUtil.RetryAsync(1, TimeSpan.Zero, new CancellationToken(), mockFn.Object, mockEx.Object);
            Assert.AreEqual(true, result);
            mockFn.Verify(_ => _(0), Times.Exactly(1));
            mockEx.Verify(_ => _(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        [TestCase(TestName = "executes result in second retry")]
        public async Task ExecutesResultWithRetry()
        {
            var mockFn = new Mock<Func<int, Task<bool>>>();
            mockFn.Setup(_ => _(0)).ThrowsAsync(new Exception("err"));
            mockFn.Setup(_ => _(1)).ReturnsAsync(true);
            var mockEx = new Mock<Action<Exception>>();
            var result = await RetryUtil.RetryAsync(2, TimeSpan.Zero, new CancellationToken(), mockFn.Object, mockEx.Object);
            Assert.AreEqual(true, result);
            mockFn.Verify(_ => _(0), Times.Exactly(1));
            mockFn.Verify(_ => _(1), Times.Exactly(1));
            mockEx.Verify(_ => _(It.IsAny<Exception>()), Times.Exactly(1));
        }

        [Test]
        [TestCase(TestName = "executes result with continue task result")]
        public async Task ExecutesResultWithContinue()
        {
            var mockFn = new Mock<Func<int, Task<bool>>>();
            mockFn.Setup(_ => _(0)).Returns(Task.FromCanceled<bool>(new CancellationToken(true)));
            mockFn.Setup(_ => _(1)).ReturnsAsync(true);
            var mockEx = new Mock<Action<Exception>>();
            var result = await RetryUtil.RetryAsync(2, TimeSpan.Zero, new CancellationToken(), mockFn.Object, mockEx.Object);
            Assert.AreEqual(true, result);
            mockFn.Verify(_ => _(0), Times.Exactly(1));
            mockFn.Verify(_ => _(1), Times.Exactly(1));
            mockEx.Verify(_ => _(It.IsAny<Exception>()), Times.Never);
        }

        [Test]
        [TestCase(TestName = "max retries reached")]
        public void MaxRetriesReached()
        {
            var mockFn = new Mock<Func<int, Task<bool>>>();
            mockFn.Setup(_ => _(0)).ThrowsAsync(new Exception("err"));
            var mockEx = new Mock<Action<Exception>>();
            Assert.ThrowsAsync<RetryUtil.RetryMaxReachedException>(async () =>
                await RetryUtil.RetryAsync(1, TimeSpan.Zero, new CancellationToken(), mockFn.Object, mockEx.Object));
            mockFn.Verify(_ => _(It.IsAny<int>()), Times.Exactly(1));
            mockEx.Verify(_ => _(It.IsAny<Exception>()), Times.Exactly(1));
        }

        [Test]
        [TestCase(TestName = "Cancellation token is cancelled")]
        public void CancellationTokenCancelled()
        {
            var cancel = new CancellationToken(true);

            var mockFn = new Mock<Func<int, Task<bool>>>();
            mockFn.Setup(_ => _(0)).ReturnsAsync(true);
            var mockEx = new Mock<Action<Exception>>();
            Assert.ThrowsAsync<RetryUtil.RetryMaxReachedException>(async () =>
                await RetryUtil.RetryAsync(1, TimeSpan.Zero, cancel, mockFn.Object, mockEx.Object));
            mockFn.Verify(_ => _(It.IsAny<int>()), Times.Never);
            mockEx.Verify(_ => _(It.IsAny<Exception>()), Times.Never);
        }
    }
}