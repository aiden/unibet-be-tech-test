using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExchangeRates.WebApi.Config;
using ExchangeRates.WebApi.Exceptions;
using ExchangeRates.WebApi.Models;
using ExchangeRates.WebApi.Services;
using ExchangeRates.WebApi.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;

namespace ExchangeRates.Tests.Tasks
{
    public class ExchangeRateUpdateTaskTest
    {
        private Mock<ILogger<ExchangeRateUpdateTask>> _mockLogger;
        private Mock<IOptions<ExchangeRateConfig>> _mockConfig;
        private Mock<IFixerApiService> _mockFixerService;
        private Mock<IExchangeRateService> _mockRateService;
        private Mock<ICacheClient> _mockRedis;
        private ExchangeRateUpdateTask _task;
        private CancellationToken _cancellationToken;

        [SetUp]
        public void Setup()
        {
            _mockLogger = new Mock<ILogger<ExchangeRateUpdateTask>>();
            _mockConfig = new Mock<IOptions<ExchangeRateConfig>>();
            _mockFixerService = new Mock<IFixerApiService>();
            _mockRateService = new Mock<IExchangeRateService>();
            _mockRedis = new Mock<ICacheClient>();
            _cancellationToken = new CancellationToken();

            var config = new ExchangeRateConfig();
            config.BaseCurrencies.Add("A");
            config.BaseCurrencies.Add("B");
            config.Fixer.ExpireDurationSeconds = 10;

            config.Currencies.Add("A");
            config.Currencies.Add("B");
            config.Currencies.Add("C");
            config.Currencies.Add("D");
            config.Fixer.MaxFetchRetries = 1;
            _mockConfig.Setup(_ => _.Value).Returns(config);

            _task = new ExchangeRateUpdateTask(_mockFixerService.Object, _mockRateService.Object, _mockConfig.Object,
                _mockRedis.Object, _mockLogger.Object);
            _task.WithBaseCurrency("A");
            _task.DelayBetweenRetries = TimeSpan.Zero;
        }

        [Test]
        [TestCase(TestName = "Fetches data when cache timestamp is past expiry policy")]
        public async Task Fetches_Data_Past_Expiry()
        {
            var response = new FixerGetLatestResponse {Base = "A", Success = true, Timestamp = DateTime.Now};
            response.Rates.Add("B", 1.1m);
            response.Rates.Add("C", 2.2m);
            response.Rates.Add("D", 3.3m);

            /**
             * All rates except for B -> n will be converted
             * since we have a base current for B and so it's rates
             * will be converted in a different background task
             */
            var keyRatesDict = new Dictionary<string, decimal>
            {
                {"A_B", 1.1m},
                {"A_C", 2.2m},
                {"A_D", 3.3m},
                {"C_A", 1m / 2.2m},
                {"D_A", 1m / 3.3m},
                {"C_D", 3.3m / 2.2m},
                {"D_C", 2.2m / 3.3m}
            };

            _mockFixerService
                .Setup(_ => _.GetLatestAsync("A", It.IsAny<IImmutableSet<string>>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);

            var result = await _task.Run().Invoke(_cancellationToken);

            Assert.AreEqual(response, result);
            _mockRedis.Verify(_ => _.AddAsync("ExchangeRateUpdateTask:A", result, TimeSpan.FromSeconds(5)), Times.Exactly(1));
            _mockFixerService.Verify(
                _ => _.GetLatestAsync("A", It.IsAny<IImmutableSet<string>>(), It.IsAny<CancellationToken>()),
                Times.Exactly(1));
            _mockRateService.Verify(_ => _.SaveExchangeRate(It.Is<ExchangeRate[]>(
                m => keyRatesDict.All(kv =>
                    m.First(v => v.Id.Equals(kv.Key) && v.Rate.Equals(kv.Value)) != null)
            )), Times.Exactly(1));
        }

        [Test]
        [TestCase(TestName = "Fetches data for second base currency")]
        public async Task Fetches_Data_For_Second_Base_Currency()
        {
            var response = new FixerGetLatestResponse {Base = "B", Success = true, Timestamp = DateTime.Now};
            response.Rates.Add("A", 1.1m);
            response.Rates.Add("C", 2.2m);
            response.Rates.Add("D", 3.3m);
            _task.WithBaseCurrency("B");
            /**
             * Only B -> n will be converted
             * since this is the second base currencies
             * and all non base currencies were previously
             * converted from first base currency
             */
            var keyRatesDict = new Dictionary<string, decimal>
            {
                {"B_A", 1.1m},
                {"B_C", 2.2m},
                {"B_D", 3.3m},
                {"C_B", 1m / 2.2m},
                {"D_B", 1m / 3.3m}
            };

            _mockFixerService
                .Setup(_ => _.GetLatestAsync("B", It.IsAny<IImmutableSet<string>>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);

            var result = await _task.Run().Invoke(_cancellationToken);

            Assert.AreEqual(response, result);
            _mockFixerService.Verify(
                _ => _.GetLatestAsync("B", It.IsAny<IImmutableSet<string>>(), It.IsAny<CancellationToken>()),
                Times.Exactly(1));
            _mockRateService.Verify(_ => _.SaveExchangeRate(It.Is<ExchangeRate[]>(
                m => keyRatesDict.All(kv =>
                    m.First(v => v.Id.Equals(kv.Key) && v.Rate.Equals(kv.Value)) != null)
            )), Times.Exactly(1));
        }

        [Test]
        [TestCase(TestName = "Fetching already is cached")]
        public async Task Fetching_Already_Cached()
        {
            var res = new FixerGetLatestResponse();
            _mockRedis.Setup(_ =>
                    _.GetAsync<FixerGetLatestResponse>("ExchangeRateUpdateTask:A", It.IsAny<CommandFlags>()))
                .ReturnsAsync(res);

            var result = await _task.Run().Invoke(_cancellationToken);
            Assert.AreSame(result, res);

            _mockRedis.Verify(_ => _.AddAsync("ExchangeRateUpdateTask:A", It.IsAny<FixerGetLatestResponse>()),
                Times.Never);
            _mockFixerService.Verify(
                _ => _.GetLatestAsync("A", It.IsAny<IImmutableSet<string>>(), It.IsAny<CancellationToken>()), Times.Never);
        }


        [Test]
        [TestCase(TestName = "Fetching fails past max retries")]
        public void Fetching_Fails()
        {
            _mockFixerService
                .Setup(_ => _.GetLatestAsync("A", It.IsAny<IImmutableSet<string>>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new Exception("some error"));
            Assert.ThrowsAsync<ExchangeRateUpdateException>(async () => await _task.Run().Invoke(_cancellationToken));
        }
    }
}