using System;
using System.Threading;
using System.Threading.Tasks;
using DalSoft.Hosting.BackgroundQueue;
using ExchangeRates.WebApi.Config;
using ExchangeRates.WebApi.Models;
using ExchangeRates.WebApi.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Moq.Protected;
using NUnit.Framework;

namespace ExchangeRates.Tests.Tasks
{
    public class ExchangeRateUpdateHostedServiceTest
    {
        private Mock<ILogger<ExchangeRateUpdateHostedService>> _mockLogger;
        private Mock<IOptions<ExchangeRateConfig>> _mockConfig;
        private Mock<BackgroundQueue> _mockBackgroundQueue;
        private Mock<IServiceProvider> _mockServices;
        private Mock<IExchangeRateUpdateTask> _mockTask;
        private ExchangeRateUpdateHostedService _service;

        [SetUp]
        public void Setup()
        {
            _mockLogger = new Mock<ILogger<ExchangeRateUpdateHostedService>>();
            _mockConfig = new Mock<IOptions<ExchangeRateConfig>>();
            _mockBackgroundQueue = new Mock<BackgroundQueue>((Action<Exception>) (e => { }), 1, 100000);
            _mockServices = new Mock<IServiceProvider>();
            _mockTask = new Mock<IExchangeRateUpdateTask>();
            var mockRun = new Mock<Func<CancellationToken, Task<FixerGetLatestResponse>>>();

            var config = new ExchangeRateConfig();
            config.BaseCurrencies.Add("A");
            config.BaseCurrencies.Add("B");
            _mockConfig.Setup(_ => _.Value).Returns(config);

            _mockTask.Setup(_ => _.WithBaseCurrency(It.IsAny<string>())).Returns(_mockTask.Object);
            _mockTask.Setup(_ => _.Run()).Returns(mockRun.Object);

            var serviceMock = new Mock<ExchangeRateUpdateHostedService>(MockBehavior.Default, _mockLogger.Object,
                _mockConfig.Object,
                _mockBackgroundQueue.Object, _mockServices.Object) {CallBase = true};
            serviceMock.Protected().Setup<IExchangeRateUpdateTask>("CreateUpdateTask").Returns(_mockTask.Object);
            _service = serviceMock.Object;
        }

        [Test]
        [TestCase(TestName = "Create scoped task for each base currency and add to background queue")]
        public void CreateScopedTasks()
        {
            _service.OnUpdate(null);
            _mockTask.Verify(_ => _.WithBaseCurrency("A"), Times.Exactly(1));
            _mockTask.Verify(_ => _.WithBaseCurrency("B"), Times.Exactly(1));
        }
    }
}