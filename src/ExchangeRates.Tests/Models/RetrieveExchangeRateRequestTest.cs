using ExchangeRates.WebApi.Models;
using NUnit.Framework;

namespace ExchangeRates.Tests.Models
{
    public class RetrieveExchangeRateRequestTest
    {
        [Test]
        [TestCase(TestName = "Currencies are converted to uppercase")]
        public void Currencies_Converted_To_Upper()
        {
            var req = new RetrieveExchangeRateRequest {BaseCurrency = "abc", TargetCurrency = "cde"};
            Assert.AreEqual("ABC", req.BaseCurrency);
            Assert.AreEqual("CDE", req.TargetCurrency);
        }
    }
}