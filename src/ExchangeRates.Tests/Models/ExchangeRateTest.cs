using System;
using ExchangeRates.WebApi.Models;
using NUnit.Framework;

namespace ExchangeRates.Tests.Models
{
    public class ExchangeRateTest
    {
        [Test]
        [TestCase(TestName = "Id is combined with BaseCurrency and TargetCurrency")]
        public void Id_Is_Created()
        {
            var rate = new ExchangeRate {BaseCurrency = "ABC", TargetCurrency = "CDE"};
            Assert.AreEqual("ABC_CDE", rate.Id);
        }

        [Test]
        [TestCase(TestName = "Rate truncated is truncated to n decimal places")]
        public void Rate_Is_Truncated()
        {
            var rate = new ExchangeRate {Rate = 1.23456789m};
            Assert.AreEqual(1.23456m, rate.RateTruncated);
        }

        [Test]
        [TestCase(TestName = "Get inversion")]
        public void Get_Inversion()
        {
            var rate = new ExchangeRate
            {
                BaseCurrency = "A", TargetCurrency = "B", Timestamp = DateTime.Now, Rate = 1.23456789m
            };
            var inv = rate.GetInversion();

            Assert.AreEqual(1m / 1.23456789m, inv.Rate);
            Assert.AreEqual("B", inv.BaseCurrency);
            Assert.AreEqual("A", inv.TargetCurrency);
            Assert.AreEqual(rate.Timestamp, inv.Timestamp);
            Assert.AreEqual("B_A", inv.Id);
        }
    }
}