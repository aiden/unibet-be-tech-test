using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ExchangeRates.WebApi.Config;
using ExchangeRates.WebApi.Models.Validation;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;

namespace ExchangeRates.Tests.Models.Validation
{
    public class CurrencySupportedTest
    {
        private class MockModel
        {
        }

        [Test]
        [TestCase(TestName = "Validates successfully")]
        public void ValidatesSuccessfully()
        {
            var config = new ExchangeRateConfig();
            config.Currencies.Add("A");
            config.Currencies.Add("B");
            var mockOpts = new Mock<IOptions<ExchangeRateConfig>>();
            mockOpts.Setup(m => m.Value).Returns(config);

            var mockProvider = new Mock<IServiceProvider>();
            mockProvider.Setup(m => m.GetService(It.IsAny<Type>())).Returns(mockOpts.Object);
            var obj = new MockModel();
            var validator = new CurrencySupported();
            var mockCtx = new ValidationContext(obj, mockProvider.Object, new Dictionary<object, object>());

            Assert.AreEqual(ValidationResult.Success, validator.GetValidationResult("A", mockCtx));
            Assert.AreEqual(ValidationResult.Success, validator.GetValidationResult("B", mockCtx));
            Assert.AreNotEqual(ValidationResult.Success, validator.GetValidationResult("C", mockCtx));
        }
    }
}